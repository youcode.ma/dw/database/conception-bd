**================MERISE=============================**

**Elaboration d'un MCD**

**Exercice I :**

Une banque désire posséder un SGBD pour suivre ses clients. Elle désire ainsi stocker les coordonnées de chaque client (nom, prénom adresse), et les comptes dont elle dispose ainsi que leur solde (sachant par ailleurs que certains comptes ont plusieurs bénéficiaires). On stockera également les opérations relatives à ces comptes (retrait et dépôt, avec leur date et le montant).
Questions :
1.	Identifier les différentes entités et leurs propriétés pour cette gestion
2.	Préciser les différentes associations entre les entités et ajouter les propriétés pour les associations porteuses de propriétés.
3.	Préciser les cardinalités pour les différentes associations.  

**Exercice II :**
Le but est de construire un  système permettant de  gérer un magasin de vente de produit a des particuliers.
Les produits du magasin possèdent une référence (un code), un libelle et un prix unitaire.
Les clients ont une identité (nom, prénom, adresse).
Les clients passent des commandes de produits. On mémorise la date de la commande.
Pour chaque commande, le client précise une adresse de livraison.
La commande concerne un certain nombre de produits, en une quantité spécifiée pour chaque produit.
Questions :
1.	Identifier les différentes entités et leurs propriétés pour cette gestion
2.	Préciser les différentes associations entre les entités et ajouter les propriétés pour les associations porteuses de propriétés.
3.	Préciser les cardinalités pour les différentes associations.  


**Exercice III: Gestion d'un cinéma**

Un cinéma désire réaliser un site web présentant le programme des films (heure de début et durée) de la semaine. Ce programme est mis à jour chaque mercredi.
Les informations contenues dans le site proviennent d'une base de donnée.
Les horaires d'un film dépendent des jours de la semaine. 
Exemple: "Il était une fois dans l'Ouest" - 18h30 - 20h (sauf mardi et mercredi) - 22h (lundi).
Chaque film est accompagné d'une photo, d'une bande annonce (film en mpg) et décrit par un résumé. Le site précise également le metteur en scène ainsi que les acteurs. Pour chaque film, le site devra publié un certain nombre de critiques de différentes sources.
Les films étrangers peuvent être en VO. Sinon, ils peuvent être traduit de deux manières: par le son ou par sous-titrage.
La base de donnée contient en outre des informations qui ne sont pas publiées sur le site: les salles attribuées à chaque film, le nombre de places disponibles dans chaque salle ainsi que le nombre d'entrées réalisées pour chaque film (total sur toutes les semaines de parution).
Les billets d'entrée pour un film ne sont plus vendus dès que la salle dans laquelle est projeté ce film est complète. Pour cela, le nombre d'entrées d'une séance est mise à jour immédiatement depuis la caisse  à chaque entrée d'un client.


**TRAVAIL À FAIRE** : Construire le Schéma Entité-Association (MCD) permettant de représenter les informations de la base de donnée de ce cinéma.

========================================================================

**Exercice I: « Gestion d’école »**
Transformez le MCD suivant, qui représente «la gestion d'une école» en un MLD en respectant toutes les règles du passage MCD au MLD.
Voir annexe

**Exercice II : « Gestion d'agence de location »**
Transformez le MCD suivant, qui représente «la gestion d'une agence de location» en un MLD en respectant toutes les règles du passage MCD au MLD.
Voir annexe

**Exercice III :   « Club de vacances»**
Transformez le MCD suivant, qui représente «la gestion d’un club de vacance» en un MLD en respectant toutes les règles du passage MCD au MLD.
Voir annexe

**================================ UML =======================================**

**1. Propriétés d'une classe**

Une personne est caractérisée par son nom, son prénom, son sexe et son âge. Les objets de classe
Personne doivent pouvoir calculer leurs revenus et leurs charges. Les attributs de la classe sont privés ; le
nom, le prénom ainsi que l'âge de la personne doivent être accessibles par des opérations publiques.
**Question** : Donnez une représentation UML de la classe Personne, en remplissant tous les compartiments adéquats.
Deux types de revenus sont envisagés : d'une part le salaire et d'autre part toutes les autres sources
de revenus. Les deux revenus sont représentés par des nombres réels (oat). Pour calculer les charges
globales, on applique un coecient xe de 20% sur les salaires et un coecient de 15% sur les autres
revenus.
**Question** : Enrichissez la représentation précédente pour prendre en compte ces nouveaux éléments.
Un objet de la classe Personne peut être créé à partir du nom et de la date de naissance. Il est possible
de changer le prénom d'une personne. Par ailleurs, le calcul des charges ne se fait pas de la même manière
lorsque la personne décède.
**Question** : Enrichissez encore la représentation précédente pour prendre en compte ces nouveaux
éléments.

**2. Relations entre classes**

Question : Pour chacun des énoncés suivants, donnez un diagramme des classes :
 Tout écrivain a écrit au moins une oeuvre
 Les personnes peuvent être associées à des universités en tant qu'étudiants aussi bien qu'en tant
que professeurs.

 Un rectangle a deux sommets qui sont des points. On construit un rectangle à partir des coordonnées
de deux points. Il est possible de calculer sa surface et son périmètre, ou encore de le translater.

 Les cinémas sont composés de plusieurs salles. Les lms sont projetés dans des salles. Les projections
correspondantes ont lieu à chacune à une heure déterminée.1

 Tous les jours, le facteur distribue des recommandés dans une zone géographique qui lui est aectée.
Les habitants sont aussi associés à une zone géographique. Les recommandés sont de deux sortes :
lettres ou colis. Comme plusieurs facteurs peuvent intervenir sur la même zone, on souhaite, pour
chaque recommandé, le facteur qui l'a distribué, en plus du destinataire.

**3 Elaboration d'un diagramme de classes complet**

Un hôtel est composé d'au moins deux chambres. Chaque chambre dispose d'une salle d'eau : douche ou
bien baignoire. Un hôtel héberge des personnes. Il peut employer du personnel et il est impérativement
dirigé par un directeur. On ne connaît que le nom et le prénom des employés, des directeurs et des
occupants. Certaines personnes sont des enfants et d'autres des adultes (faire travailler des enfants est
interdit). Un hôtel a les caractéristiques suivantes : une adresse, un nombre de pièces et une catégorie.
Une chambre est caractérisée par le nombre et de lits qu'elle contient, son prix et son numéro. On veut
pouvoir savoir qui occupe quelle chambre à quelle date. Pour chaque jour de l'année, on veut pouvoir
calculer le loyer de chaque chambre en fonction de son prix et de son occupation (le loyer est nul si la
chambre est inoccupée). La somme de ces loyers permet de calculer le chire d'aaires de l'hôtel entre
deux dates.
**Question** : Donnez une diagramme de classes pour modéliser le problème de l'hôtel.
